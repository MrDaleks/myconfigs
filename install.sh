#!/bin/bash



######## Variables

dir=~/myconfigs
files="vimrc vim"

########


for file in $files; do
	
	# Copy new files
	echo "Copy $file -> ~/.$file"
	if [ -d $file ]
	then
		cp -Rf $dir/$file ~/.$file
	else
		cp $dir/$file ~/.$file
		echo ""
	fi
done
